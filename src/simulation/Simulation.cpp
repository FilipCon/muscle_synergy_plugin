#include "Settings.h"

#include <OpenSim/OpenSim.h>
#include <MuscleSynergyOptimization.h>

using namespace OpenSim;
using namespace SimTK;
using namespace std;

void run() {
    cout << "Not implemented! :P" << endl;
}

int main(int argc, char* argv[]) {
    try {
        run();
    } catch (exception& e) {
        cout << typeid(e).name() << ": " << e.what() << endl;
        return -1;
    }
    return 0;
}