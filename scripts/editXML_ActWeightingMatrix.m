function editXML_ActWeightingMatrix(file, new_file, synergies)
%% Edit Synergy Optimization Setup File - Example Inputs
% file = 'C:\SO_Files\Setup_SO.xml';
% new_file = 'new_SOsetup.xml';

%% Read XML
Pref.NoCells = false;
XML_Tree = xml_read(file,Pref);

%% Edit XML
% Change synergies
for i = 1:size(synergies,2)
    XML_Tree.AnalyzeTool.AnalysisSet.objects.MuscleSynergyOptimization.ActWeightingMatrix.objects.ActWeightingVector(i).array = synergies(:,i)';
    XML_Tree.AnalyzeTool.AnalysisSet.objects.MuscleSynergyOptimization.ActWeightingMatrix.objects.ActWeightingVector(i).ATTRIBUTE.name = ['w' num2str(i)];
end
% Delete unused synergies
if size(synergies,2) < size(XML_Tree.AnalyzeTool.AnalysisSet.objects.MuscleSynergyOptimization.ActWeightingMatrix.objects.ActWeightingVector,2)
    XML_Tree.AnalyzeTool.AnalysisSet.objects.MuscleSynergyOptimization.ActWeightingMatrix.objects.ActWeightingVector(size(synergies,2)+1:end) = [];
end

%% Write XML File
Pref.XmlEngine='Xerces';
Pref.StructItem=false;
Pref.CellItem=false;
Pref.RootOnly=true;

RootComment = 'Generated from Matlab XML Tree v1.0';
RootName = {'OpenSimDocument','',RootComment};
xml_writeOSIM(new_file, XML_Tree, RootName, Pref);