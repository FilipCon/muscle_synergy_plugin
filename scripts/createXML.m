%% SYNERGY OPTIMIZATION SAMLE
%   Goal: Sample code for creating the set-up (.xml file) for the synergy
%   optimization plug-in for OpenSim
%   Author: Kat M. Steele
%   Date: 1/12/2016
%
%   Ouptut: Set-up file (.xml) with user-specified activation weighting
%   matrix for use with synergy optimization plug-in.

%   Inputs (example for Models>Arm26.osim)
n_actuators = 6;                % Total number of actuators in model
file_in = 'arm26_Setup_SynergyOptimization.xml';        % Sample set-up file
file_out = 'arm26_Setup_SynergyOptimization_EDIT.xml';  % Name of new output file

%   User-specific activation weighting matrix (choose one)
    % 1. Identity Matrix (results identical to Static Optimization)
    ActWeightingMatrix = eye(n_actuators);
    % 2. Penalize BIClong activation (actuator #4 in model, give weight of 0.1
    % ActWeightingMatrix = eye(n_actuators); 
    % ActWeightingMatrix(4,4) = 0.1;
    % 3. Make the BIClong & BICshort have same activation
    % ActWeightingMatrix = eye(n_actuators);
    % ActWeightingMatrix(5,4) = 1; % Add BICshort (Actuator #5)
    % ActWeightingMatrix(:,5) = []; % Delete BICshort column

%   Write matrix to XML file
editXML_ActWeightingMatrix(file_in, file_out, ActWeightingMatrix);
